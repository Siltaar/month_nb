// Name: month_nb.js
// SPDX-FileCopyrightText: 2013-2022 Simon Descarpentres <simon /\ acoeuro [] com>, 2021 Christopher Gauthier <Christopher /\ christopher-gauthier [] com>
// SPDX-License-Identifier: GPL-3.0-only
// Desc: returns month number from given litteral case-insensitive name or abbreviation
// Version: 4
/* globals month_nb_json */

export const month_nb = function (a, json_obj, check_keys=false) {
	json_obj = json_obj ? json_obj : month_nb_json
	let keys = [Object.keys(json_obj)]
	let objs = [json_obj]
	let o_ks = keys[keys.length-1]
	let o = objs[objs.length-1]
	let r_range = ''
	let str_r_range = ''
	while(o_ks.length) {
		let k = o_ks.shift()
		if (k == "comment") continue
		// console.log(a +' '+ k)
		if (check_keys) { // show chars out of lang range in keys
			if (keys.length == 1) {
				str_r_range = `${k.slice(1,k.length-1)}|[\\$\\.\\?\\^\\(\\)\\|\\[\\] ])`
				r_range = new RegExp(str_r_range, 'gi')
			} else if (keys.length > 1) {
				let char_to_remove = k.replace(r_range, '')
				if (char_to_remove.length > 1) {
					console.log(str_r_range)
					console.log(`You should remove ${char_to_remove} from ${k}`)
				}
			}
		}
		let r = new RegExp(k, 'i')
		if (r.test(a)) {
			o = o[k]
			if (typeof(o) === 'number') {
				return o
			} else if (typeof(o) === 'object') {
				objs.push(o)
				o_ks = Object.keys(o)
				keys.push(o_ks)
			} else {
				throw 'The JSON language-regexp-tree structure should only contain string '+
					'keys as nodes, sub-objects as branchs and number leafs'
			}
		} else if (o_ks.length == 0) {
			objs.pop() // backtrack
			o = objs[objs.length-1]
			keys.pop() // console.log(`depth level ${keys.length}`)
			if (keys.length) {
				o_ks = keys[keys.length-1] // console.log(`keys to explore ${o_ks.length}`)
			} else { // console.error(`Did not found for ${a}.`)
				return undefined
			}
		} else {
			continue
		}
	}
}
