# Name: month_nb.py
# SPDX-FileCopyrightText: 2013-2021 Simon Descarpentres <simon /\ acoeuro [] com>, 2021 Christopher Gauthier <Christopher /\ christopher-gauthier [] com>
# SPDX-License-Identifier: GPL-3.0-only

import json, re

# read file
with open('../../month_nb.json', 'r') as myfile:
	data = myfile.read()

# parse file
global_json_obj = json.loads(data)


def month_nb(month_name, json_obj=None):
	json_obj = json_obj if json_obj else global_json_obj
	month_name = str(month_name) if type(month_name) is int else month_name
	keys = list(json_obj.keys())
	objs = [json_obj]
	o_ks = keys
	o = objs[-1]
	k = ''
	while type(o_ks) is list and len(o_ks):
		k = o_ks.pop(0)
		if k == "comment":
			objs.pop(0)
			continue
		if re.match(k, month_name, re.IGNORECASE):
			o = o[k]
			if type(o) is int:
				return o
			elif type(o) is dict:
				objs.append(o)
				o_ks = list(o.keys())
				keys.append(o_ks)
			else:
				raise Exception('The JSON language-regexp-tree structure should only contain '
						'string keys as nodes, sub-objects as branchs and number leafs')
		elif len(o_ks) == 0:
			if len(objs) > 1:
				objs.pop()
				o = objs[-1]
				keys.pop()
				if len(keys):
					o_ks = keys[-1]
				else:
					return None
			else:
				print(objs)
				return None
		else:
			pass
