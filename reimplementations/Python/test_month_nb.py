#!/bin/env python3
import json
from month_nb import month_nb

# read file
with open('../../month_definitions/2019_definitions.json', 'r') as myfile:
	data = myfile.read()

# parse file
month_definitions = json.loads(data)
month_index = 0
month_name = ''
broken_lang = ['Abkhaz', 'Bashkir', 'Buryat', 'Cherokee', 'Chuvash', 'Erzya', 'Hawaiian',
	'Kalmyk', 'Lezgi', 'Moksha', 'Navajo', 'Ojibwe', 'Old English', 'Old High German',
	'Old Norse', 'Tibetan', 'Udmurt', 'Uyghur', 'Veps', 'Vietnamese', 'Welsh', 'Yakut',
	'Yiddish', 'Zazaki']

working_lang_nb = 0

for lang, month_names in month_definitions.items():
	if lang in broken_lang:
		continue
	print(lang, ':', ' '.join([a[:3].rjust(3) for a in month_names]))
	print(' '*(len(lang)+2), end='')
	month_index = 0
	month_value = 0
	while(month_index < 12):
		month_name = month_names[month_index]
		month_value = month_index + 1
		month_nb_res = month_nb(month_name)
		if not month_nb_res == month_value:
			print(f"{month_name} gave {month_nb_res} instead of {month_value}")
			broken_lang.append(lang)
			break
		print(f'{month_nb_res: 3}', end=' ')
		month_index += 1
	else:
		print('')
		working_lang_nb += 1
print('Total :', len(broken_lang) + working_lang_nb, 'over 98')
print('Working lang. :', working_lang_nb)
print('Broken lang. :', len(broken_lang))
print(', '.join(broken_lang))
